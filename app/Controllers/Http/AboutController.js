'use strict'

class AboutController {
  index({request, view}){
    return view.render('about');
  }
}

module.exports = AboutController
