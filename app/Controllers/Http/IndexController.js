'use strict'

class IndexController {
  index({request,view}){
    return view.render('welcome');
  }
}

module.exports = IndexController
