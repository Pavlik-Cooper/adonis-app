'use strict'

class ContactController {
  index({request,view}){
    return view.render('contact');
  }
}

module.exports = ContactController
