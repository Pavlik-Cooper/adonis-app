'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route');

Route.get('/','IndexController.index');

Route.get('/contact',({request,view})=>{
  if (true){
    return view.render('view');
  }
});

Route.get('/about','AboutController.index');
Route.get('/signup','SignupController.index');
Route.get('/hi','HiController.index');
Route.get('/by','ByController.index');
